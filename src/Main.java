public class Main {

    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.makeSounds();
        Dog dog = new Dog();
        dog.makeSounds();
        Cow cow = new Cow();
        cow.makeSounds();
    }


}